
*Задача*  

+ Есть несколько директорий a1 a2 a3 a4 b1 b2 b5 cc  
+ Необходимо посчитать кол-во файлов во всех директориях кроме сс  
+ И записать в файл count.file (кол-во файлов для проверки создайте сами)  

---

*Решение*

1. обзор среды выполнения:   

```
root@ub01:/home/ln# tree bash/
bash/
├── a1
│   ├── sdfgh
│   ├── sdfgh2
│   ├── sdfgsdehr
│   └── sdfgsdwertju
├── a2
│   ├── sdfgsdehr
│   └── sdfgsdwertju
├── a3
│   ├── gssdgfrdwertju
│   └── sdfgssdgfrdwertju
├── a4
│   ├── gssdasdfggfrdwertju
│   └── gssdasdfju
├── b1
│   └── gssdasdfju
├── b2
│   ├── gasdf
│   ├── gasdfgssdasdfju
│   └── gasdfsedrhsdfju
├── b5
│   └── gasdf
└── cc
    ├── jlkasg
    └── jlkasglk.jbgaeq
8 directories, 17 files
root@ub01:/home/ln#
```

2.  выполняю прямо из папки bash:  

вариант 1 с использованием ls: 
 
```
for i in *
  do
if [ $i != "cc" ]; then
     echo $i: $(ls -1 $i | wc -l) | tee -a ~/count.file
  fi
done
```

вариант 2 через массив:     

```
for i in *
  do
if [ $i != "cc" ]; then
    b=( $(ls $i/) ) && echo $i: "${#b[@]}" | tee -a ~/count.file
  fi
done
```

Результат выполнения варианта 1  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/zRVrzcr/1.jpg" alt="1" border="0"></a>

Результат выполнения варианта 2  

<a href="https://imgbb.com/"><img src="https://i.ibb.co/vDXJMsM/2.jpg" alt="2" border="0"></a>
